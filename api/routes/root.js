'use strict'
const _ = require('lodash');
const { irr } = require('node-irr')
const baseUrl = '/api'

module.exports = async function (fastify, opts) {
  const connection = await fastify.mysql.getConnection()


  /////////////// hello world
  fastify.get(`${baseUrl}`, async function (request, reply) {
    return 'Hello world'
  })


  /////////////// get all
  fastify.get(`${baseUrl}/indices`, async (request, reply) => {
    const [result, fields] = await connection.query(
      'SELECT * from indices', [request.params.id],
    )
    if (result.length === 0) {
      throw new Error('No documents found')
    }
    return result
  })


  /////////////// get one
  fastify.get(`${baseUrl}/indices/:id`, async (request, reply) => {
    const [result, fields] = await connection.query(
      'SELECT * from indices where id=?', [request.params.id],
    )
    if (result.length === 0) {
      throw new Error('Índice não encontrado')
    }
    return result
  })


  /////////////// historico
  fastify.get(`${baseUrl}/indices/:id/historico`, async (request, reply) => {
    let result = {
      indice: {},
      historico: []
    }

    const [indice, fields0] = await connection.query(
      'SELECT * from indices where id=?', [request.params.id],
    )
    if (indice.length === 0) {
      throw new Error('Índice não encontrado')
    }

    const [historico, fields] = await connection.query(
      'SELECT mes, ano, valor, variacao from valores where indiceId=?', [request.params.id],
    )
    if (historico.length === 0) {
      throw new Error('Índice não tem valores históricos')
    }

    result.indice = indice[0];
    result.historico = historico;

    return result
  })


  /////////////// correcao
  fastify.post(`${baseUrl}/indices/:id/correcao`, async (request, reply) => {
    // busca no db o índice
    const [indice, fields0] = await connection.query(
      'SELECT * from indices where id=?', [request.params.id],
    )
    if (indice.length === 0) {
      throw new Error('Índice não encontrado')
    }

    // busca no db os valores do índice
    const [historico, fields] = await connection.query(
      'SELECT mes, ano, valor, variacao from valores where indiceId=?', [request.params.id],
    )
    if (historico.length === 0) {
      throw new Error('Índice não tem valores históricos')
    }

    // aqui comeca a magica
    // console.log('\n Aqui comeca a magica')

    var mesInicial = request.body[0].mes
    var anoInicial = request.body[0].ano
    var mesFinal = (new Date()).getMonth() + 1
    var anoFinal = (new Date()).getFullYear()

    var qteMeses = (anoFinal - anoInicial) * 12;
    qteMeses += mesFinal + 1;
    qteMeses -= mesInicial;
    qteMeses <= 0 ? 0 : qteMeses;

    var result = []
    var totalCorrigido = 0
    var totalSemCorrecao = 0
    var mesAtual = mesInicial
    var anoAtual = anoInicial


    // console.log(request.body)
    for (let i = 1; i <= qteMeses; i++) {
      let linha = {
        mes: mesAtual,
        ano: anoAtual,
        variacao: null,
        movimento: null,
        correcaoMesAnterior: null,
        valorCorrigido: null
      }

      // verifica se existe movimento para a data atual
      let linhaEntrada = {}
      linhaEntrada = request.body.find(el => (el.ano === anoAtual & el.mes === mesAtual))
      if (linhaEntrada) linha.movimento = _.round(linhaEntrada.movimento, 2)
      else linha.movimento = 0

      // verifica se existe indice para a data atual
      let indiceAtual = {}
      indiceAtual = historico.find(el => (el.ano === anoAtual & el.mes === mesAtual))
      if (indiceAtual) linha.variacao = _.round(indiceAtual.variacao, 2)

      // total sem correcao
      totalSemCorrecao += linha.movimento

      // corrige valor
      linha.correcaoMesAnterior = _.round((linha.variacao / 100) * totalCorrigido, 2)
      totalCorrigido = totalCorrigido + linha.correcaoMesAnterior + linha.movimento
      linha.valorCorrigido = _.round(totalCorrigido, 2)


      // console.log(linha)

      result.push(linha)

      // incrementa data atual
      mesAtual += 1
      if (mesAtual > 12) {
        mesAtual = 1
        anoAtual += 1
      }

    }

    return {
      qteMeses: qteMeses,
      totalSemCorrecao: _.round(totalSemCorrecao, 2),
      totalCorrecao: _.round(totalCorrigido - totalSemCorrecao, 2),
      totalCorrigido: _.round(totalCorrigido, 2),
      indice: indice,
      resultado: result
    }



  })


  /////////////// correcao taxa fixa
  fastify.post(`${baseUrl}/correcaotaxafixa`, async (request, reply) => {

    // aqui comeca a magica
    // console.log('\n Aqui comeca a magica')

    // console.log(request.body)
    var variacao = request.body.variacao
    var fluxo = request.body.fluxo

    var mesInicial = fluxo[0].mes
    var anoInicial = fluxo[0].ano
    var mesFinal = (new Date()).getMonth() + 1
    var anoFinal = (new Date()).getFullYear()

    var qteMeses = (anoFinal - anoInicial) * 12;
    qteMeses += mesFinal + 1;
    qteMeses -= mesInicial;
    qteMeses <= 0 ? 0 : qteMeses;

    var result = []
    var totalCorrigido = 0
    var totalSemCorrecao = 0
    var mesAtual = mesInicial
    var anoAtual = anoInicial


    // console.log(request.body)
    for (let i = 1; i <= qteMeses; i++) {
      let linha = {
        mes: mesAtual,
        ano: anoAtual,
        variacao: null,
        movimento: null,
        correcaoMesAnterior: null,
        valorCorrigido: null
      }

      // verifica se existe movimento para a data atual
      let linhaEntrada = {}
      linhaEntrada = fluxo.find(el => (el.ano === anoAtual & el.mes === mesAtual))
      if (linhaEntrada) linha.movimento = _.round(linhaEntrada.movimento, 2)
      else linha.movimento = 0

      // verifica se existe indice para a data atual
      // let indiceAtual = {}
      // indiceAtual = historico.find(el => (el.ano === anoAtual & el.mes === mesAtual))
      // if (indiceAtual) linha.variacao = _.round(indiceAtual.variacao, 2)
      linha.variacao = variacao

      // total sem correcao
      totalSemCorrecao += linha.movimento

      // corrige valor
      linha.correcaoMesAnterior = _.round((linha.variacao / 100) * totalCorrigido, 2)
      totalCorrigido = totalCorrigido + linha.correcaoMesAnterior + linha.movimento
      linha.valorCorrigido = _.round(totalCorrigido, 2)


      // console.log(linha)

      result.push(linha)

      // incrementa data atual
      mesAtual += 1
      if (mesAtual > 12) {
        mesAtual = 1
        anoAtual += 1
      }

    }

    return {
      qteMeses: qteMeses,
      totalSemCorrecao: _.round(totalSemCorrecao, 2),
      totalCorrecao: _.round(totalCorrigido - totalSemCorrecao, 2),
      totalCorrigido: _.round(totalCorrigido, 2),
      resultado: result
    }



  })


  /////////////// taxa de retorno
  fastify.post(`${baseUrl}/taxaretorno`, async (request, reply) => {
    // calcula irr
    const taxaRetorno = irr(request.body)

    return {
      irr: _.round(taxaRetorno * 100, 4) + '%',
    }
  })


  /////////////// taxa de retorno com data
  fastify.post(`${baseUrl}/taxaretornocomdata`, async (request, reply) => {
    var data = request.body
    console.log(data)

    var mesInicial = data[0].mes
    var anoInicial = data[0].ano
    var mesFinal = data[data.length - 1].mes
    var anoFinal = data[data.length - 1].ano

    var qteMeses = (anoFinal - anoInicial) * 12;
    qteMeses += mesFinal + 1;
    qteMeses -= mesInicial;
    qteMeses <= 0 ? 0 : qteMeses;

    console.log(`${mesInicial}/${anoInicial} ~ ${mesFinal}/${anoFinal}: ${qteMeses} meses`)

    var fluxo = []
    var mesAtual = mesInicial
    var anoAtual = anoInicial
    for (let i = 1; i <= qteMeses; i++) {
      // verifica se existe movimento para a data atual
      let linhaEntrada = {}
      linhaEntrada = request.body.find(el => (el.ano === anoAtual & el.mes === mesAtual))
      if (linhaEntrada) fluxo.push(_.round(linhaEntrada.movimento, 2))
      else fluxo.push(0)

      // incrementa data atual
      mesAtual += 1
      if (mesAtual > 12) {
        mesAtual = 1
        anoAtual += 1
      }
    }

    // calcula irr
    const taxaRetorno = irr(fluxo)

    return {
      irr: _.round(taxaRetorno * 100, 4) + '%',
      fluxo,
    }
  })

}
